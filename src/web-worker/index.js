const worker = new Worker('work.js')
worker.postMessage('Hello world')
worker.postMessage({
  method: 'echo',
  args: ['work']
})
worker.onmessage = function (event) {
  console.log('receive: ' + event.data)
  doSomething()
}

function doSomething () {
  worker.postMessage('Work done')
}
