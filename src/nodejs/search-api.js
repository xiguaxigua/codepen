const sa = require('superagent')
let pointer = 1
const max = 10000

main()

function main () {
  sa.get(`http://adca-dt-amelia-1.vm.elenet.me:8080/template/placeholders/${pointer}`).end(res => {
    console.log(res)
    if (res.body.data) {
      process.stdout.write(`[${pointer}]`)
    } else {
      process.stdout.write(pointer)
    }
    if (pointer < max) {
      pointer++
      main()
    }
  })
}
