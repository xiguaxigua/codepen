### [post] /amelia/getStatistics/{taskType}

> taskType = push/message/crm/gift
> 金额数据前端做四舍五入

#### req

- pageNum   页码
- pageSize  页数
- beginDate 查询开始日期
- endDate   查询结束日期
- taskId    任务id
- l1Depart  业务来源-一级部门名称
- l2Depart  业务来源-二级部门名称
- l3Depart  业务来源-三级部门名称

#### res

```json
{
  "code": 200,
  "message": "ok",
  "data": {
    "total": 1, // 总记录数
    "rows": [
      {
        "sendDate": "2017-10-14 09:30:00", // 发送日期
        "taskId": "2385", // 任务id
        "businessSource": "测试L1", // 业务来源。显示这个，可能为空
        "l1Dept": "测试L1",
        "l2Dept": "",
        "l3Dept": "",
        "title": "发丰富的萨", // 标题
        "content": "啊发的说法", // 内容
        "progress": {
          "total": 95000, // 发送用户
          "readNum": 5100, // 打开用户
          "receivedNum": 30000,
          "sentNum": 95000,
          "sentSucesNum": 85000, // 发送成功数
          "userNum": 7500, // 转化用户
          "orderAmount": 248556, // 转化金额
          "oneHourUserNum": 5900, // 消费用户
          "oneHourAmout": 158002, // 消费金额
          "andriodReceivedTotal": 40000,
          "sentPercent": "71.76%", // 发送率
          "readPercent": "6.00%", // 打开率
          "translatePercent": "147.06%", // 转化率
          "consumePercent": "115.69%", // 消费率
          "sentSucesPercent": "89.47%", // 发成率
          "messageCost": 1862, //  这个push不要显示
          "updatedAt": 1508036260572
        },
        "progressStr": ""
      }
    ]
  }
}
```
