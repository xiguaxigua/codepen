function isArray (arg) {
  return Object.prototype.toString.call(arg) === '[object Array]'
}

function createRandomColor () {
  return '#' + ('00000' + (Math.random()*0x1000000<<0).toString(16)).slice(-6)
}

function camelToKebab (string) {
  return string.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
}

function camelToUnderscore (string) {
  return string.match(/^[a-z][a-z0-9]+|[A-Z][a-z0-9]*/g).join('_').toLowerCase();
}

function queryStringToJson (search) {
  if (search === void 0) { search = ''; }
  return (function(querystring) {
    if (querystring === void 0) { querystring = ''; }
    return (function(q) {
      return (querystring.split('&').forEach(function(item) {
        return (function(kv) {
          return kv[0] && (q[kv[0]] = kv[1]);
        })(item.split('='));
      }), q);
    })({});
  })(search.split('?')[1]);
}

function getQueryString(key){
  var reg = new RegExp("(^|&)"+ key +"=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if(r!=null){
      return  unescape(r[2]);
  }
  return null;
}

function flatten (arr) {
  JSON.parse(`[${JSON.stringify(arr).replace(/\[|]/g, '')}]`)
}

function dateFormat (fmt) {
  var o = {
    "M+": this.getMonth() + 1,
    "d+": this.getDate(),
    "h+": this.getHours(),
    "m+": this.getMinutes(),
    "s+": this.getSeconds(),
    "q+": Math.floor((this.getMonth() + 3) / 3),
    "S": this.getMilliseconds()
  };
  if (/(y+)/.test(fmt)){
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o){
    if (new RegExp("(" + k + ")").test(fmt)){
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }
  }
  return fmt;
}
