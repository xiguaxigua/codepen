var F = F || {};

F.define = function (str, fn) {
  var parts = str.split('.');
  var old = parent = this;
  var i = len = 0;

  if (parts[0] === 'F') parts = parts.slice(1);

  if (parts[0] === 'define' || parts[0] === 'module') return;

  for (len = parts.length; i < len; i++) {
    if (typeof parent[parts[i]] === 'undefined') parent[parts[i]] = {}; 
    old = parent;
    parent = parent[parts[i]];
  }

  if (fn) {
    old[parts[--i]] = fn();
  }

  return this;
}

F.module = function () {
  var args = [].slice.call(arguments);
  var fn = args.pop();
  var parts = args[0] && args[0] instanceof Array ? args[0] : args;
  var modules = [];
  var modIds = '';
  var i = 0;
  var iLen = parts.length;
  var parent;
  var j;
  var jLen;

  while (i < iLen) {
    if (typeof parts[i] === 'string') {
      parent = this;
      modIds = parts[i].replace(/^F\./, '').split('.');

      for (j = 0, jLen = modIds.length; j < jLen; j++) {
        parent = parent[modIds[j]] || false;
      }

      modules.push(parent);
    } else {
      modules.push(parts[i])
    }
    i++;
  }
  fn.apply(null, modules)
}

F.define('string', function () {
  return {
    trim: function (str) {
      return str.replace(/^\s+|\s+$/g, '');
    }
  }
})

F.define('dom', function () {
  var $ = function (id) {
    $.dom = document.getElementById(id);
    return $;
  }

  $.html = function (html) {
    if (html) {
      this.dom.innerHTML = html
      return this
    } else {
      return this.dom.innerHTMl
    }
  }
  return $;
})

console.log(F.string.trim(' 测试用例 '));

F.module(['dom', document], function (dom, doc) {
  dom('test').html('new add!');
  doc.body.style.background = 'red';
})
