/*function* helloWorldGenerator () {
  yield 'hello'
  yield 'world'
  return 'ending'
}

var hw = helloWorldGenerator()

console.log(hw.next())
console.log(hw.next())
console.log(hw.next())
console.log(hw.next())*/
function* foo () {
  yield 1
  yield 2
  yield 3
  yield 4
  yield 5
  yield 6
  return 7
}

for (let v of foo()) {
  console.log(v)
}
