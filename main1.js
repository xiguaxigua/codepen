var fs = require('fs')

var walk = function (dir, done) {
  var results = []
  console.log(1)
  fs.readdir(dir, function (err, list) {
    if (err) return done(err)
    var i = 0;
    (function next () {
      var file = list[i++]
      if (!file) return done(null, results)
      file = dir + '/' + file
      fs.stat(file, function (err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function (err, res) {
            results = results.concat(res)
            next()
          })
        } else {
          results.push(file)
          next()
        }
      })
    })()
  })
}

walk('./public', function (err, results) {
  if (err) throw err
  var str = results.map(item => {
    var targetPath = item.replace(/^.\/public/, '')
    return [
      '<a href="' + targetPath + '">',
      targetPath,
      '</a>'
    ].join('')
  }).join('</br>')
  fs.writeFile('./public/index.html', str, function (err) {
    if (err) console.log(err)
  })
})
